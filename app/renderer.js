// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
// function GetProcess(type) {
//     process.versions[type];
// 

const { shell } = require("electron");

const parser = new DOMParser();

function getProcess(type) {
    return process.versions[type];
}

function setVersionValue(type) {
    $('#' + type + '-version').text(getProcess(type));
}

const clearForm = function()
{
    $('#inputUrl').val("");
};

const parseResponse = function(text)
{
    return parser.parseFromString(text,'text/html');
};

const findTitle = function(nodes)
{
    return nodes.querySelector('title').innerText;
};

const storeLink = function(title,url)
{
    return localStorage.setItem(url,JSON.stringify({title:title,url:url}));
};


const getLinks = function()
{
    return Object.keys(localStorage).map(key=> JSON.parse(localStorage.getItem(key)) );
};

const convertToElement = function(link)
{
    return "<a href="+link.url+" class='list-group-item list-group-item-action linkItem'><h3>"+link.title+"</h3><p>"+link.url+"</p></a>";
};

const renderLinks = function()
{
    const linkElements = getLinks().map(convertToElement).join('');

    $("#lstLink").html(linkElements);
};


const handlerError = function(error,url)
{
    // alert(error);
    // const linkElements = getLinks().map(convertToElement).join('');
    $("#errorMessage").show();
    $("#errorMessage").html("添加链接'"+url+"',错误信息："+error.message+"");

    setTimeout(
        function() {
            $("#errorMessage").hide();
        },
        5000
    );
};

const validateResponse = function(response)
{
    if (response.ok) { return response}
    
    throw new Error(' Status code '+response.status+' '+response.statusText+' ');
};


$(document).ready(function () {
    $('#errorMessage').hide();
    setVersionValue('node');
    setVersionValue('chrome');
    setVersionValue('electron');
    $('#rootPath').text(__dirname);
    $('#inputUrl').keyup(function(){
        var disableSumbit = !$(this)[0].validity.valid;
        console.log(disableSumbit);
        if(disableSumbit)
        {
            $( "#btnSubmit" ).removeClass( "btn-primary" );
            $( "#btnSubmit" ).addClass( "btn-secondary" );
        }
        else
        {
            $( "#btnSubmit" ).removeClass( "btn-secondary" );
            $( "#btnSubmit" ).addClass( "btn-primary" );
        }
    });
    $('#btnSubmit').click(function(){
        if(!$('#inputUrl')[0].validity.valid)
            return;

        var url = $('#inputUrl').val();

        fetch(url)
            .then(response => response.text())
            .then(parseResponse)
            .then(findTitle)
            .then(title => storeLink(title,url))
            .then(clearForm)
            .then(renderLinks)
            .catch(error => handlerError(error,url))
        ; 
    });

    $(".linkItem").click(function(event){

        event.preventDefault();

        console.log(event);

        if(event.target.href)
        {
            console.log(event.target.href);

            shell.openExternal(event.target.href);
        }

      });

    // $('.linkItem').click(function(event){

    //     alert(event);

    //     console.log(event);

    //     if(event.target.href)
    //     {
    //         event.preventDefault();
    //         shell.openExternal(event.target.href);
    //     }

    // });


    $('#btnClear').click(function(){

        localStorage.clear();

        $("#lstLink").html('');

    });

    
});

renderLinks();

